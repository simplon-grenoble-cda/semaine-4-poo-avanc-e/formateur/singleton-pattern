# 1 - Problème

On représente des acteurs (classe `Actor`) et des companies de théatre (classe `Company`). 

Description des modèles : 

- Acteurs : name (`String`), age (`int`)
- Company : name (`String`), actors (`List<Actor>`)

On veut un système qui permette de génerer des Companies (instances de `Company`). On va utiliser le pattern Factory pour celà.

Détail du fonctionnement de notre factory : 

- On donne le nom voulu pour la companie en paramètre
- La factory affecte aléatoirement 3 acteurs à la companie
- Ces acteurs proviennent d'une liste d'acteurs prédéfinie : 
  - John, 26 ans
  - Bob, 30 ans
  - Alice, 41 ans
  - Susan, 52 ans
  - David, 31 ans
  - Michel, 62 ans
- On veut que dans notre programme, il n'y ait qu'une seule instance de `Actor` pour représenter un acteur donné, on ne souhaite
  pas dupliquer les instances.


On rajoute des méthodes utilitaires `print()` pour afficher facilement nos objets sur la console. Pour la classe `Actor` : 

```
    public void print() {
        System.out.printf("%s (%d)\n", this.name, this.age);
    }
```

Pour la classe `Company`

```
    public void print() {
        System.out.println("============");
        System.out.printf("This is the company : %s\n", this.name);
        System.out.println("It has the following actors : ");
        for (Actor actor : this.actors) {
            actor.print();
        }
        System.out.println("============\n");
    }
```


Bien sur vous ajouter les méthodes utilitaires nécessaires (constructeurs, getter, setters, …).

Une fois que ces classes sont implémentées, on fait un petit `main`, où on va créer une instance de `Company` avec 3 acteurs aléatoires,
grace à notre Factory, puis on affiche la représentation texte de cette companie à la console.

## 2 - Délégation du choix d'un acteur aléatoire

Pour séparer les problématiques, on va créer une classe utilitaire afin d'extraire certaines fonctionnalités de la Factory.
La classe s'appelle `ExistingActorsRepository`, et possède la méthode `public Actor pickRandomActor()`.

C'est cette classe qui va contenir la liste des acteurs existants, et la méthode `pickRandomActor()` permet d'obtenir la référence
vers une instance d'`Actor` de notre liste.

Implémentez cette classe, et refactorez la Factory pour qu'elle utilise cette nouvelle classe.


À ce stade, votre code devrait être similaire à celui du dossier `solution_step2`. **Ne regardez pas la solution** avant d'avoir fait
l'exercice, le but est que vous compreniez les concepts, pas de copier mon code.

## 3 - On veut créer 2 companies

On veut créer deux companies au lieu d'une dans notre `main`.

**Attention :** on ne veut surtout pas dupliquer les instances de `Actor`. Et donc a priori ne pas dupliquer les instances
de `ExistingActorsRepository`.

## 4 - Une autre classe qui nous sert à afficher le contenu du repository des acteurs

On va créer une classe "helper", avec une méthode static "printActorsRepositorySummary()" qui prend en paramètre l'instance
du ActorsRepository et en affiche le contenu.

Dans notre `main` on veut désormais non seulement créer deux companies et afficher leur contenu, mais également afficher
grace à ce helper le contenu du ActorsRepository.


## 5 - Le problème de la gestion de l'insance unique de ActorsRepository


Le problème : 

- plusieurs éléments du code ont besoin de la même instance de notre classe ExistingActorsRepository : celà nécessite de passer cette instance partout
  où il y en a besoin
- on risque de créer deux instances de ExistingActorsRepository, confusion dans le code (selon notre logique applicative, il n'y a pas de raisons 
  d'en avoir deux instances)

Mettre en place un pattern Singleton (recherche perso à faire), l'objectif est de s'assurer qu'à travers l'application il n'est possible de n'avoir
qu'une seule et unique instance de ExistingActorsRepository. Et par ailleurs d'avoir une fonctionnalité permettant d'obtenir facilement la référence
vers cette instance sans avoir à la passer en paramètre à chaque objet ou méthode qui en a besoin.
