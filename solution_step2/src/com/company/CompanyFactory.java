package com.company;

import java.util.ArrayList;
import java.util.List;

public class CompanyFactory {
    private List<Actor> actorsRepository;

    public CompanyFactory() {
        this.actorsRepository = new ArrayList<>();
        this.actorsRepository.add(new Actor("John", 26));
        this.actorsRepository.add(new Actor("Bob", 20));
        this.actorsRepository.add(new Actor("Alice", 41));
        this.actorsRepository.add(new Actor("Susan", 52));
        this.actorsRepository.add(new Actor("David", 31));
        this.actorsRepository.add(new Actor("Michel", 62));
    }

    public Company createCompanyWith3RandomActors(String companyName) {
        ExistingActorsRepository existingActorsRepository = new ExistingActorsRepository();
        Company company = new Company(companyName);
        for (int i = 0; i < 3; i++) {
            company.addActor(existingActorsRepository.pickRandomActor());
        }

        return company;
    }
}
