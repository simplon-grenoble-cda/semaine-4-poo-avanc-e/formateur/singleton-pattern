package com.company;

import java.util.ArrayList;
import java.util.List;

public class Company {
    protected String name;
    protected List<Actor> actors;

    public Company() {
        this.actors = new ArrayList<>();
    }

    public Company(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void addActor(Actor actor) {
        this.actors.add(actor);
    }

    public void print() {
        System.out.println("============");
        System.out.printf("This is the company : %s\n", this.name);
        System.out.println("It has the following actors : ");
        for (Actor actor : this.actors) {
            actor.print();
        }
        System.out.println("============\n");
    }
}
