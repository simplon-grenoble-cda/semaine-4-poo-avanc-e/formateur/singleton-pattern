package com.company;

public class Main {

    public static void main(String[] args) {

        /** Create a company with 3 random actors */
        CompanyFactory companyFactory = new CompanyFactory();
        Company myCompany = companyFactory.createCompanyWith3RandomActors("Théatre de Lyon");
        myCompany.print();
    }
}
