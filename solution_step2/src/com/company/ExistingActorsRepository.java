package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExistingActorsRepository {
    private List<Actor> existingActorsList;

    public ExistingActorsRepository() {
        this.existingActorsList = new ArrayList<>();
        this.existingActorsList.add(new Actor("John", 26));
        this.existingActorsList.add(new Actor("Bob", 20));
        this.existingActorsList.add(new Actor("Alice", 41));
        this.existingActorsList.add(new Actor("Susan", 52));
        this.existingActorsList.add(new Actor("David", 31));
        this.existingActorsList.add(new Actor("Michel", 62));
    }

    public Actor pickRandomActor() {
        Random randomGenerator = new Random();
        int randomIndex = randomGenerator.nextInt(this.existingActorsList.size());
        Actor randomActor = this.existingActorsList.get(randomIndex);
        return randomActor;
    }
}
