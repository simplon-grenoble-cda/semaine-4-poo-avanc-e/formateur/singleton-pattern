package com.company;

public class Actor {
    protected String name;
    protected int age;

    public Actor(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Actor() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void print() {
        System.out.printf("%s (%d)\n", this.name, this.age);
    }
}
